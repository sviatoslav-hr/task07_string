package com.khrystyna.views;

/**
 * Implementation of the class is used to show specified info.
 */
@FunctionalInterface
public interface View {
    /**
     * Runs the view.
     */
    void run();
}
