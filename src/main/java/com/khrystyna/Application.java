package com.khrystyna;

import com.khrystyna.model.task1.RegexService;
import com.khrystyna.model.task1.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.regex.Matcher;

/**
 * Program main start point.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 02.05.2019
 */
public class Application {

    /**
     * Utility class should not have a public constructor.
     */
    private Application() {
    }

    private static Logger logger = LogManager.getLogger(Application.class);

    /**
     * Main method that runs when the program is started.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        StringUtils stringUtils = new StringUtils("new", 1, 3.0,
                new int[]{1, 2, 3}, new int[]{3, 4, 5});
        logger.trace(stringUtils);

        RegexService regexService = new RegexService();

        String sentence = "This is a nice day. Have Fun! " +
                "Can you visit gooogle.com?";
        Matcher matcher = regexService.matchSentence(sentence);

        while (matcher.find()) {
            logger.trace(sentence.substring(matcher.start(), matcher.end()));
        }

        logger.trace(Arrays.toString(regexService
                .split("This is the sentence. " +
                        "The first one. There is smth, no?)")));

    }
}
