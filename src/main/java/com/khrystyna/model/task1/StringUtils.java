package com.khrystyna.model.task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class with an undefined number of parameters of any class
 * that concatenates all parameters and returns Strings.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 02.05.2019
 */
public class StringUtils {
    /**
     * Parameters list.
     */
    private List<Object> params = new ArrayList<>();

    /**
     * Constructs object and fills list with objects of any type.
     *
     * @param params objects of any type
     */
    public StringUtils(final Object... params) {
        this.params.addAll(Arrays.asList(params));
    }

    @Override
    public final String toString() {
        return "[ "
                + params.stream().map(Object::toString)
                        .collect(Collectors.joining(", "))
                + " ]";
    }
}
