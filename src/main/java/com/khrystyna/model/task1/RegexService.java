package com.khrystyna.model.task1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple service that uses regular expressions for solving tasks.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 02.05.2019
 */
public class RegexService {

    /**
     * Sentence sentencePattern that checks if a sentence
     * begins with a capital letter and ends with a period.
     * Also allows to type site addresses using period.
     */
    private Pattern sentencePattern = Pattern
            .compile("[A-Z]\\w*([\\w]+\\.[\\w]+)*[^.!?]*[.!?](\\n| |$)");
    /**
     *
     */
    private Pattern questionSentencePattern = Pattern
            .compile("[A-Z][^.!?]*([\\w]+\\.[\\w]+)*[^.!?]*[?](\\n| |$)");

    /**
     * @param str string for matching
     * @return {@link Matcher} for string using sentencePattern.
     */
    public final Matcher matchSentence(final String str) {
        return sentencePattern.matcher(str);
    }

    /**
     * @param str string for matching
     * @return {@link Matcher} for string using questionSentencePattern.
     */
    public final Matcher matchQuestionSentence(final String str) {
        return questionSentencePattern.matcher(str);
    }

    /**
     * Splits the string on the words 'the' or 'you'.
     *
     * @param str string to split
     * @return splitted strings array
     */
    public final String[] split(final String str) {
        return str.split("([ \\t\\n]([Tt]he)|([Yy]ou))[ \\t\\n,.?!:]");
    }
}
