package com.khrystyna.model.task2.text;

import java.util.LinkedList;
import java.util.List;

/**
 * An instance of this class is used to store sentences list.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 02.05.2019
 */
public class Text {
    /**
     * List of sentences.
     */
    private List<Sentence> sentences = new LinkedList<>();

    /**
     * @return list of sentences
     */
    public final List<Sentence> getSentences() {
        return sentences;
    }

    /**
     * Adds new sentence to list.
     *
     * @param sentence sentence to be added
     */
    public final void addSentence(final Sentence sentence) {
        sentences.add(sentence);
    }
}
