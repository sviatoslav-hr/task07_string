package com.khrystyna.model.task2;

import com.khrystyna.model.task1.RegexService;
import com.khrystyna.model.task2.text.Sentence;
import com.khrystyna.model.task2.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

public class TextService {
    public static char[] VOWELS = {
            'a', 'e', 'i', 'o', 'u', 'y'};
    public static char[] CONSONANTS = {'b', 'c', 'd', 'f', 'g', 'h', 'j',
            'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'};
    private static Logger logger = LogManager.getLogger(TextService.class);

    private Text text;
    private RegexService regexService = new RegexService();

    TextService(String text) {
        this.text = getText(fixText(text));
    }

    TextService(File file) {
        StringBuilder string = new StringBuilder();
        try (FileReader reader = new FileReader(file)) {
            Scanner scanner = new Scanner(reader);
            while (scanner.hasNext()) {
                string.append(scanner.nextLine());
            }
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
        }

        this.text = getText(fixText(string.toString()));
    }

    /**
     * Returns {@link Text} object formed by dividing {@link String} parameter
     * into sentences.
     *
     * @param str string to look for sentences
     * @return text formed by string
     */
    private Text getText(String str) {
        Matcher matcher = regexService.matchSentence(str);
        Text text = new Text();
        while (matcher.find()) {
            String sentence = str.substring(matcher.start(), matcher.end());
            text.addSentence(new Sentence(sentence.trim()));
        }
        return text;
    }

    /**
     * Replaces all redundant tabs and spaces with one space.
     *
     * @param text string to be fixed
     * @return string without redundant tabs and spaces
     */
    private String fixText(String text) {
        return text.replaceAll("[\\t ]+", " ");
    }

    /**
     * Returns sentence with max number of same words.
     *
     * @return string value of sentence with max number of same words.
     */
    public String getMaxSameWordsSentence() {
        Sentence max = null;
        String mostRepeated = null;
        for (Sentence sentence : text.getSentences()) {
            String sentenceMax = getMostRepeatedWord(sentence);
            if (max == null
                    || sentence.wordEntries(sentenceMax) > max
                    .wordEntries(mostRepeated)) {
                mostRepeated = sentenceMax;
                max = sentence;
            }
        }
        if (max == null) {
            return null;
        }
        return max.toString();
    }

    /**
     * Returns the most repeated word for sentence.
     *
     * @param sentence sentence to look for
     * @return the most repeated word
     */
    private String getMostRepeatedWord(Sentence sentence) {
        List<String> words = sentence.getWords();
        String mostRepeated = null;
        for (String word : words) {
            if (mostRepeated == null
                    || sentence.wordEntries(word) > sentence
                    .wordEntries(mostRepeated)) {
                mostRepeated = word;
            }
        }
        return mostRepeated;
    }

    /**
     * @return list of sentences sorted by words number.
     */
    public List<String> getSortedByWordsNumber() {
        return text.getSentences().stream()
                .sorted(Comparator.comparingInt((Sentence o) -> o.getWords().size())
                        .thenComparing(Sentence::toString))
                .map(Sentence::toString)
                .collect(Collectors.toList());
    }

    /**
     * @return word in the first sentence that is not in any of other
     * sentences
     */
    public Set<String> getUniqueWords() {

        List<String> allWords = text.getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .map(word -> word.toLowerCase().trim())
                .collect(Collectors.toList());
        System.out.println(allWords.toString());
        return allWords.stream()
                .filter(word -> allWords.stream().filter(s -> s.equals(word)).count() == 1)
                .collect(Collectors.toSet());
    }

    /**
     * @param length length of words to find
     * @return words of given length in question sentences
     */
    public List<String> getWordsOfGivenLengthInQuestionSentences(int length) {
        return text.getSentences().stream()
                .filter(sentence -> regexService
                        .matchQuestionSentence(sentence.toString()).matches())
                .map(Sentence::getWords)
                .flatMap(List::stream)
                .filter(s -> s.length() == length)
                .collect(Collectors.toList());
    }

    /**
     * @return list of sentences where first words starting with vowel
     * was replaced with the longest word.
     */
    public List<String> replaceFirstVowelWordsWithLongest() {
        return text.getSentences().stream()
                .map(sentence -> {
                    List<String> words = sentence.getWords();
                    Optional<String> max = words.stream()
                            .max(Comparator.comparingInt(String::length));
                    Optional<String> first = words.stream()
                            .filter(s -> {
                                for (char vowel : VOWELS) {
                                    if (s.substring(0, 1).equalsIgnoreCase("" + vowel)) {
                                        return true;
                                    }
                                }
                                return false;
                            })
                            .findFirst();
                    if (max.isPresent() && first.isPresent()) {
                        return sentence.toString()
                                .replace(first.get(), max.get());
                    } else {
                        return sentence.toString();
                    }
                })
                .collect(Collectors.toList());
    }

    /**
     * @return list of words sorted in alphabetical order.
     */
    public List<String> getSortedWordsAsc() {
        return text.getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .map(s -> s.substring(0, 1).toUpperCase() + s.substring(1))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * @return list of words sorted in vowels percentage order.
     */
    public List<String> getSortedWordsByVowelPercentage() {
        return text.getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .map(s -> s.substring(0, 1).toUpperCase() + s.substring(1))
                .distinct()
                .sorted(new VowelsPercentageComparator())
                .collect(Collectors.toList());
    }

    /**
     * @return list of words that starts with vowel letter
     * sorted by first consonant letter.
     */
    public List<String> getVowelWordsSorted() {
        return text.getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .map(s -> s.substring(0, 1).toUpperCase() + s.substring(1))
                .distinct()
                .filter(s -> {
                    s = s.toLowerCase();
                    for (char vowel : VOWELS)
                        if (s.charAt(0) == vowel)
                            return true;
                    return false;
                })
                .sorted(new FirstConsonantComparator())
                .collect(Collectors.toList());
    }

    /**
     * @return list of words sorted by number of given letter occurrences.
     * If numbers are equal, sort in alphabetical order.
     */
    public List<String> getWordsSortedByCharNum(char letter) {
        if (!checkIfLetter(letter)) {
            throw new RuntimeException("Character is not a letter!");
        }
        return text.getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .map(s -> s.substring(0, 1).toUpperCase() + s.substring(1))
                .distinct()
                .sorted((o1, o2) -> {
                    int res = (o1.length() - o1.replace("" + letter, "").length())
                            - (o2.length() - o2.replace("" + letter, "").length());
                    if (res == 0) {
                        return o1.compareTo(o2);
                    } else {
                        return res;
                    }
                })
                .collect(Collectors.toList());
    }

    private boolean checkIfLetter(char c) {
        for (char vowel : VOWELS) {
            if (c == vowel)
                return true;
        }
        for (char consonant : CONSONANTS) {
            if (c == consonant)
                return true;
        }
        return false;
    }

    /**
     * @return list of words sorted by number of occurrence in text.
     */
    public List<String> getWordsSortedByOccurrence() {
        return text.getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .map(s -> s.substring(0, 1).toUpperCase() + s.substring(1))
                .distinct()
                .sorted(new WordOccurrenceComparator())
                .collect(Collectors.toList());
    }

    private class VowelsPercentageComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            double v1 = getVowelsPercentage(o1);
            double v2 = getVowelsPercentage(o2);
            System.out.println(o1 + v1 + " " + o2 + v2);
            double res = v2 - v1;
            if (res < 0) {
                return -1;
            } else if (res > 0) {
                return 1;
            } else {
                return o1.compareTo(o2);
            }
        }

        private double getVowelsPercentage(String s) {
            int vowelsNum = 0;
            double length = s.length();
            s = s.toLowerCase();
            for (int i = 0; i < s.length(); i++) {
                for (char vowel : VOWELS) {
                    if (s.charAt(i) == vowel) {
                        vowelsNum++;
                    }
                }
            }
            return vowelsNum / length;
        }
    }

    private class FirstConsonantComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            int cons1 = consonantIndex(o1);
            int cons2 = consonantIndex(o2);
            if (cons1 < 0 || cons2 < 0) {
                return cons1 - cons2;
            }
            return o1.substring(cons1).compareTo(o2.substring(cons2));
        }

        private int consonantIndex(String s) {
            s = s.toLowerCase();
            for (int i = 0; i < s.length(); i++) {
                for (char consonant : CONSONANTS) {
                    if (s.charAt(i) == consonant) {
                        return i;
                    }
                }
            }
            return -1;
        }
    }

    private class WordOccurrenceComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            return getOccurrenceNumber(o2) - getOccurrenceNumber(o1);
        }

        private int getOccurrenceNumber(String findString) {
            int count = 0;
            for (Sentence sentence : text.getSentences()) {
                for (String word : sentence.getWords()) {
                    if (word.equalsIgnoreCase(findString)) {
                        count++;
                    }
                }
            }
            return count;
        }
    }
}
